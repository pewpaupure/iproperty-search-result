import React from "react";

import './header.css';
import { ReactComponent as HeaderLogoSvg } from './assets/icons/iproperty-logo.svg';


function HeaderLogo(props) {
  return (
    <div className="header-logo">
      <HeaderLogoSvg className="header-logo__svg"></HeaderLogoSvg>
    </div>
  );
}

export default class Header extends React.Component {
  render() {
    return (
      <div className="header">  
        <HeaderLogo></HeaderLogo>
      </div>
    );
  }
}
