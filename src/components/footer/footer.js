import React from "react";

import "./footer.css";
import LocalLinks from "./components/local-links/local-links";


export default class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <LocalLinks></LocalLinks>
            </div>
        );
    }
}