import React from "react";

import "./local-links.css";

const localLinkOptions = [
  {
    link: "",
    label: "About Us",
  },
  {
    link: "",
    label: "Careers",
  },
  {
    link: "",
    label: "Contact Us",
  },
  {
    link: "",
    label: "Advertise with Us",
  },
  {
    link: "",
    label: "Privacy Policy",
  },
  {
    link: "",
    label: "Website Terms",
  },
  {
    link: "",
    label: "Terms and Conditions",
  },
];

class LocalLinkItem extends React.Component {
  render() {
    return (
      <li className="local-link-item">
        <a href={this.props.link}>{this.props.label}</a>
      </li>
    );
  }
}

export default class LocalLinks extends React.Component {
  render() {
    const localLinkItems = localLinkOptions.map((option) => (
      <LocalLinkItem
        link={option.link}
        label={option.label}
        key={option.label}
      ></LocalLinkItem>
    ));

    return <ul className="local-links">{localLinkItems}</ul>;
  }
}
