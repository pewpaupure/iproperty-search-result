// React Imports
import React from "react";
import ReactDOM from "react-dom";

import "./index.css";
import Header from "./components/header/header";
import SearchResultPage from './pages/search-result/search-result-page';
import Footer from "./components/footer/footer";


class AppRoot extends React.Component {
  render() {
    return (
      <div className="app-root">
        <Header></Header>

        <div className="content">
          <SearchResultPage></SearchResultPage>
        </div>
        
        <Footer></Footer>
      </div>
    )
  }
}

ReactDOM.render(<AppRoot />, document.getElementById("root"));
