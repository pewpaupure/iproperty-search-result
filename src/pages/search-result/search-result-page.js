import React from "react";

import SearchBox from "./components/search-box/search-box";
import SearchResult from "./components/search-result/search-result";

export default class SearchResultPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  componentDidMount() {
    fetch("https://run.mocky.io/v3/58071f32-2239-4868-bc4e-f706a377bdb7")
      .then((response) => response.json())
      .then(
        (result) => {
          const parsed_items = result.items.map(
            item => ({
              thumbnailUrl: item.cover?.thumbnailUrl,
              fallback: item.cover?.urlTemplate,
              price: item.prices?.[0].min,
              currency: item.prices?.[0].currency,
              lister: item.listers?.[0].name,
              title: item.title,
              formattedAddress: item.address?.formattedAddress,
              propertyType: item.propertyType,
              builtUp: item.attributes?.builtUp,
            })
          )

          this.setState({
            isLoaded: true,
            items: parsed_items,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
  }

  render() {
    const { error, isLoaded, items } = this.state;
    let searchResult = null;

    if (error) {
      searchResult = <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      searchResult = <div>Loading...</div>;
    } else {
      searchResult = <SearchResult items={items}></SearchResult>;
    }

    return (
      <div className="search-result-page">
        <SearchBox></SearchBox>

        {searchResult}
      </div>
    );
  }
}
