import React from "react";

import "./search-result.css";
import ResultCard from "./components/result-card/result-card";

export default class SearchResult extends React.Component {
  render() {
    const resultCards = this.props.items.map((item) => (
      <ResultCard
        thumbnailUrl={item.thumbnailUrl}
        fallback={item.fallback}
        price={item.price}
        currency={item.currency}
        lister={item.lister}
        title={item.title}
        formattedAddress={item.formattedAddress}
        propertyType={item.propertyType}
        builtUp={item.builtUp}
      ></ResultCard>
    ));
    return (
      <div className="search-result">
        {resultCards}
      </div>
    );
  }
}
