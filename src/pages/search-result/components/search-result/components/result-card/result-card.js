import React from "react";

import { Card, Image, Statistic } from "antd";
import { StarOutlined } from "@ant-design/icons";

import "./result-card.css";

const { Meta } = Card;

export default class ResultCard extends React.Component {
  render() {
    const thumbnail = (
      <Image
        src={this.props.thumbnailUrl}
        fallback={this.props.fallback}
      ></Image>
    );
    const price = (
      <Statistic
        value={this.props.price}
        prefix={this.props.currency}
      ></Statistic>
    );

    return (
      <Card
        className="result-card"
        title={this.props.lister}
        cover={thumbnail}
        actions={[<StarOutlined></StarOutlined>]}
      >
        <Meta title={price} />
        <br></br>
        <h3>{this.props.title}</h3>
        {this.props.formattedAddress}
        <br></br>
        {this.props.propertyType}&nbsp;•&nbsp;Built-up :
        {this.props.builtUp} sq. ft.
      </Card>
    );
  }
}
