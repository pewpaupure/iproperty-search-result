export const mockData = {
  "channels": ["sale"],
  "kind": "property",
  "id": "sale-6820470",
  "shareLink": "https://www.iproperty.com.my/property/arcoris-soho-mont-kiara-premium-6820470",
  "title": "Arcoris Soho, Mont Kiara",
  "active": true,
  "tier": 3,
  "propertyType": "Serviced Residence",
  "prices": [{
    "type": "sale",
    "currency": "MYR",
    "max": 750000,
    "min": 750000
  }],
  "cover": {
    "type": "image",
    "url": "https://pictures-my.ippstatic.com/realtors/images/640/34543/8c42876c5d7f4a6aa067d1dcb5679849.jpg",
    "thumbnailUrl": "https://pictures-my.ippstatic.com/realtors/images/640/34543/8c42876c5d7f4a6aa067d1dcb5679849.jpg",
    "urlTemplate": "https://img.rea-asia.com/my-subsale/premium/${width}x${height}-${scale}/realtors/images/640/34543/8c42876c5d7f4a6aa067d1dcb5679849.jpg"
  },
  "medias": [{
    "type": "image",
    "url": "https://pictures-my.ippstatic.com/realtors/images/640/34543/8c42876c5d7f4a6aa067d1dcb5679849.jpg",
    "thumbnailUrl": "https://pictures-my.ippstatic.com/realtors/images/640/34543/8c42876c5d7f4a6aa067d1dcb5679849.jpg",
    "urlTemplate": "https://img.rea-asia.com/my-subsale/premium/${width}x${height}-${scale}/realtors/images/640/34543/8c42876c5d7f4a6aa067d1dcb5679849.jpg"
  }, {
    "type": "image",
    "url": "https://pictures-my.ippstatic.com/realtors/images/640/34543/254507b01fe64f699dd32c6bad583d7d.jpg",
    "thumbnailUrl": "https://pictures-my.ippstatic.com/realtors/images/640/34543/254507b01fe64f699dd32c6bad583d7d.jpg",
    "urlTemplate": "https://img.rea-asia.com/my-subsale/premium/${width}x${height}-${scale}/realtors/images/640/34543/254507b01fe64f699dd32c6bad583d7d.jpg"
  }, {
    "type": "image",
    "url": "https://pictures-my.ippstatic.com/realtors/images/640/34543/c56ddc9ceb89455888b3b6cc869fa25c.jpg",
    "thumbnailUrl": "https://pictures-my.ippstatic.com/realtors/images/640/34543/c56ddc9ceb89455888b3b6cc869fa25c.jpg",
    "urlTemplate": "https://img.rea-asia.com/my-subsale/premium/${width}x${height}-${scale}/realtors/images/640/34543/c56ddc9ceb89455888b3b6cc869fa25c.jpg"
  }, {
    "type": "image",
    "url": "https://pictures-my.ippstatic.com/realtors/images/640/34543/2126c12819754e8f940997184576a257.jpg",
    "thumbnailUrl": "https://pictures-my.ippstatic.com/realtors/images/640/34543/2126c12819754e8f940997184576a257.jpg",
    "urlTemplate": "https://img.rea-asia.com/my-subsale/premium/${width}x${height}-${scale}/realtors/images/640/34543/2126c12819754e8f940997184576a257.jpg"
  }, {
    "type": "image",
    "url": "https://pictures-my.ippstatic.com/realtors/images/640/34543/b1968959446b49f09ca94e6be89e9f32.jpg",
    "thumbnailUrl": "https://pictures-my.ippstatic.com/realtors/images/640/34543/b1968959446b49f09ca94e6be89e9f32.jpg",
    "urlTemplate": "https://img.rea-asia.com/my-subsale/premium/${width}x${height}-${scale}/realtors/images/640/34543/b1968959446b49f09ca94e6be89e9f32.jpg"
  }, {
    "type": "image",
    "url": "https://pictures-my.ippstatic.com/realtors/images/640/34543/f0b0239a15444c9eb499bca44ab80410.jpg",
    "thumbnailUrl": "https://pictures-my.ippstatic.com/realtors/images/640/34543/f0b0239a15444c9eb499bca44ab80410.jpg",
    "urlTemplate": "https://img.rea-asia.com/my-subsale/premium/${width}x${height}-${scale}/realtors/images/640/34543/f0b0239a15444c9eb499bca44ab80410.jpg"
  }],
  "updatedAt": "2019-05-09T09:45:00.000Z",
  "publishedAt": "2019-05-09T09:45:14+00:00",
  "address": {
    "formattedAddress": "Jalan Kiara 4",
    "lat": 3.166783,
    "lng": 101.651499
  },
  "multilanguagePlace": {
    "en-GB": {
      "level1": "Kuala Lumpur",
      "level2": "Mont Kiara",
      "level3": "Arcoris Soho"
    }
  },
  "referenceCode": "UP6820470",
  "attributes": {
    "bathroom": "1",
    "bedroom": "1",
    "carPark": "1",
    "builtUp": "700",
    "landTitleType": "Residential",
    "tenure": "Freehold",
    "unitType": "SOHO",
    "sizeUnit": "SQUARE_FEET"
  },
  "listers": [{
    "id": "34543",
    "type": "agent",
    "name": "Kira Chow",
    "contact": {
      "phones": [{
        "label": "Mobile",
        "number": "+60122885564"
      }]
    }
  }],
  "organisations": [{
    "id": "1801",
    "type": "agency",
    "name": "Private Advertiser"
  }]
}