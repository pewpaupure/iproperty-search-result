import React from "react";

import "./search-box.css";
import StateSelect from "./components/state-select/state-select";
import SearchAutoSuggest from "./components/search-autosuggest/search-autosuggest";
import ChannelSwitchers from "./components/channel-switchers/channel-switchers";

export default class SearchBox extends React.Component {
  render() {
    return (
      <div className="search-box">
        <StateSelect></StateSelect>
        <SearchAutoSuggest></SearchAutoSuggest>
        <ChannelSwitchers></ChannelSwitchers>
      </div>
    );
  }
}
