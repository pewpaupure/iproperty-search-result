import React from "react";

import { Select } from "antd";

const { Option } = Select;
const stateSelectOptions = [
  "All States",
  "Selangor",
  "Kuala Lumpur",
  "Johor",
  "Penang",
  "Perak",
  "Negeri Sembilan",
  "Melaka",
  "Pahang",
  "Sabah",
  "Sarawak",
  "Kedah",
  "Putrajaya",
  "Kelantan",
  "Terengganu",
  "Perlis",
  "Labuan",
];

export default class StateSelect extends React.Component {
  render() {
    const options = stateSelectOptions.map((option) => (
      <Option className="state-select-option" value={option} key={option}>
        {option}
      </Option>
    ));

    return (
      <Select
        className="state-select"
        defaultValue={stateSelectOptions[0]}
        size="large"
      >
        {options}
      </Select>
    );
  }
}
