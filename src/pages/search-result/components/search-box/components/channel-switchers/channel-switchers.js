import React from "react";

import { Radio } from "antd";

const channelOptions = [
  {
    label: "For Sale",
    value: "for-sale",
  },
  {
    label: "For Rent",
    value: "for-rent",
  },
];

class ChannelOption extends React.Component {
  render() {
    return (
      <Radio className="channel-option" value={this.props.value}>
        {this.props.label}
      </Radio>
    );
  }
}

export default class ChannelSwitchers extends React.Component {
  render() {
    const channelOptionRadios = channelOptions.map((option) => (
      <ChannelOption
        label={option.label}
        value={option.value}
        key={option.value}
      ></ChannelOption>
    ));

    return (
      <Radio.Group
        className="channel-switchers"
        options={channelOptions}
        optionType="button"
        buttonStyle="solid"
        size="large"
        defaultValue={channelOptions[0].value}
      >
        {channelOptionRadios}
      </Radio.Group>
    );
  }
}
