import React from "react";

import { Select } from "antd";


export default class SearchAutoSuggest extends React.Component {
    render() {
        return (
          <Select
            className="search-autosuggest"
            mode="multiple"
            size="large"
            placeholder="Search by Locations, Trains stations, Neighbourhoods, or Property name"
          >
          </Select>
        );
    }
}